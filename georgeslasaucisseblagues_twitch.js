import https from 'https';
import tmi from 'tmi.js';

const timeBeforeAnswer = 5000;

const tmiConfig = {
  connection: {
    reconnect: true,
  },
  identity: {
    username: 'NOM_DU_BOT',
    password: 'COLLEZ_LE_JETON_ICI',
  },
  channels: ['CHAINE_1', 'CHAINE_2'],
};

const client = new tmi.client(tmiConfig);

client.connect().catch(console.error);

const options = {
  host: 'www.georgeslasaucisse.fr',
  path: '/api/blague',
  headers: { 'User-Agent': 'request' },
};

client.on('chat', (channel, user, message, isSelf) => {
  if (isSelf) {
    return;
  }
  if (message === '!blague') {
    https.get(options, (res) => {
      let json = '';
      res.on('data', (chunk) => {
        json += chunk;
      });
      res.on('end', () => {
        let data = {};
        let blague = '';
        let reponse = '';
        if (res.statusCode === 200) {
          try {
            data = JSON.parse(json);
            blague = data.blague.replace(/\n/g, ' ');
            client.say(channel, blague);
            if (data.reponse !== '') {
              reponse = data.reponse.replace(/\n/g, ' ');
              setTimeout(() => {
                client.say(channel, reponse);
              }, timeBeforeAnswer);
            }
          } catch (e) {
            console.log(`Parse JSON error : ${e}`);
          }
        } else {
          console.log(`Status : ${res.statusCode}`);
        }
      });
    }).on('error', (err) => {
      console.log(`HTTPS Error : ${err}`);
    });
  }
});

process.on('error', (err) => { console.log(err); });
