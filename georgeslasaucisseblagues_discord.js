import https from 'https';
import Discord from 'discord.js';

const token = 'COLLEZ_LE_JETON_ICI';
const timeBeforeAnswer = 5000;
const timeBeforeNextBlague = 10000;

const client = new Discord.Client();

const options = {
  host: 'www.georgeslasaucisse.fr',
  path: '/api/',
  headers: { 'User-Agent': 'request' },
};

const optionsBlague = Object.assign({}, options);
optionsBlague.path += 'blague';

const optionsSerieBlagues = Object.assign({}, options);
optionsSerieBlagues.path += 'serieblagues';

function blague(msg, data) {
  msg.channel.send(data.blague).catch(console.error);
  if (data.reponse !== '') {
    setTimeout(() => {
      msg.channel.send(data.reponse).catch(console.error);
    }, timeBeforeAnswer);
  }
}

client.login(token);

client.on('message', (msg) => {
  if ((msg.content === '!blague') || (msg.content === '!serieblagues')) {
    let opt = {};
    switch (msg.content) {
      case '!blague':
        opt = optionsBlague;
        break;
      case '!serieblagues':
        opt = optionsSerieBlagues;
        break;
      default:
        break;
    }
    https.get(opt, (res) => {
      let json = '';
      res.on('data', (chunk) => {
        json += chunk;
      });
      res.on('end', () => {
        let data = {};
        let i = 1;
        let interval = '';
        if (res.statusCode === 200) {
          try {
            data = JSON.parse(json);
            if (Array.isArray(data)) {
              blague(msg, data[0]);
              interval = setInterval(() => {
                blague(msg, data[i]);
                i += 1;
                if (!data[i]) {
                  clearInterval(interval);
                }
              }, timeBeforeNextBlague);
            } else {
              blague(msg, data);
            }
          } catch (e) {
            console.log(`Parse JSON error : ${e}`);
          }
        } else {
          console.log(`Status : ${res.statusCode}`);
        }
      });
    }).on('error', (err) => {
      console.log(`HTTPS Error : ${err}`);
    });
  }
});

process.on('error', (err) => { console.log(err); });
