# GEORGESLASAUCISSEBLAGUES

GeorgesLaSaucisseBlagues est un bot Node.js pour messageries instantanées (pour l'instant disponible pour Discord et Twitch) faisant partie de l'univers de [Georges la Saucisse](https://www.georgeslasaucisse.fr/). Il suffit de poster le message `!blague` dans un salon, et le bot retournera une de ses blagues prises au hasard dans l'API de blagues Georges la Saucisse. S'il reçoit une blague de type question-réponse, il attendra quelques secondes avant de poster la réponse, pour laisser le temps aux membres du salon de deviner la chute.

Le bot Discord dispose d'une commande supplémentaire, `!serieblagues`, permettant de retourner toutes les blagues d'une même série, avec un temps d'attente entre chaque post d'une blague.

## 1. Utilisation avec installation

Il est nécessaire de télécharger et d'installer Node.js sur votre ordinateur/serveur. [Cette page du tutoriel Node.js d'OpenClassrooms](https://openclassrooms.com/fr/courses/1056721-des-applications-ultra-rapides-avec-node-js/1056956-installer-node-js) explique la procédure d'installation sur Windows, Mac OS X et Linux, et les bases du fonctionnement du terminal.

Téléchargez ensuite [l'ensemble de GeorgesLaSaucisseBlagues](https://gitlab.com/Desmu/GeorgesLaSaucisseBlagues/-/archive/master/GeorgesLaSaucisseBlagues-master.zip), décompressez le .ZIP, et rangez le dossier obtenu sur votre ordinateur (il sera nécessaire de redémarrer les bots si le dossier ou ses fichiers sont déplacés, choisissez un emplacement sur votre ordinateur qui ne sera pas amené à changer si possible).

Depuis le terminal, placez vous ensuite dans le dossier GeorgesLaSaucisseBlagues-master (il est possible de le renommer), en tapant `cd CHEMIN_DU_DOSSIER` (exemple sur Windows : `cd \Users\Moi\Desktop\mon_dossier_sur_le_bureau\GeorgesLaSaucisseBlagues-master`) puis en appuyant sur Entrée. Laissez ensuite la fenêtre du terminal ouverte.

### 1.1. Bot Discord

#### 1.1.1. Création du profil du bot sur Discord.

Sur votre navigateur favori, rendez-vous sur [la page de création de bot Discord](https://discord.com/developers/applications), cliquez sur "Nouvelle application". Nommez le bot "GeorgesLaSaucisseBlaguesBOT" (ou tout autre nom que vous souhaitez lui donner, ce sera celui qui apparaîtra sur vos serveurs). Ajoutez une description et un avatar si vous le souhaitez, puis cliquez sur "Créer".

Cliquez ensuite sur "Bot" dans le menu, et cliquez sur "Créez un utilisateur Bot". Confirmez, puis cliquez sur "Cliquer pour révéler" au niveau du champ "Jeton". Copiez la suite de chiffres et de lettres qui apparaît alors.

#### 1.1.2. Lancement du bot.

Ouvrez le fichier `georgeslasaucisseblagues_discord.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et collez la suite de chiffres et de lettres à la ligne 4, entre les apostrophes à la place de COLLEZ_LE_JETON_ICI. Enregistrez et fermez le fichier.

Depuis le terminal laissé ouvert depuis tout à l'heure, tapez `npm install discord.js` puis Entrée (le bot utilise la librairie [Discord.js](https://discord.js.org/) pour fonctionner). À la fin de l'installation, tapez enfin `node georgeslasaucisseblagues_discord.js` et laissez la fenêtre du terminal ouverte, pour que le bot se connecte à Discord.
En fonction de votre version de Node.js, il sera peut-être nécessaire de taper plutôt `node --experimental-modules georgeslasaucisseblagues_discord.js`.

### 1.2. Bot Twitch

#### 1.2.1. Création du profil du bot sur Twitch.

Commencez par créer un compte Twitch normal pour le bot, en lui donnant le nom de votre choix (vous pouvez réutiliser la même adresse mail pour plusieurs comptes). Gardez ce nom de côté.

Restez connecté à ce compte, rendez-vous sur [la page de génération de mot de passe OAuth Twitch](https://twitchapps.com/tmi/), cliquez sur "Connect" et autorisez l'accès au compte. Copiez la suite de chiffres et de lettres débutant par oauth: qui apparaît alors. Vous pouvez ensuite vous déconnecter du compte du bot.

#### 1.2.2. Lancement du bot.

Ouvrez le fichier `georgeslasaucisseblagues_twitch.js` dans un éditeur de texte comme Bloc-Notes (évitez les logiciels de bureautique qui pourraient provoquer des problèmes à l'enregistrement), et collez la suite de chiffres et de lettres (avec la mention oauth:) à la ligne 12, entre les apostrophes à la place de COLLEZ_LE_JETON_ICI.

Placez ensuite le nom donné à votre bot, entre les apostrophes à la ligne 11, à la place de NOM_DU_BOT. Enfin, ajoutez les noms des chaînes auxquelles vous souhaitez que le bot se connecte à la ligne 14 (à la place de CHAINE_1, CHAINE_2, ...). Chaque nom de chaîne doit être renseigné entre apostrophes et suivi d'une virgule (sauf pour le dernier nom renseigné). Enregistrez et fermez le fichier.

Depuis le terminal laissé ouvert depuis tout à l'heure, tapez `npm install tmi.js` puis Entrée (le bot utilise la librairie [TMI.js](https://tmijs.com/) pour fonctionner). À la fin de l'installation, tapez enfin `node georgeslasaucisseblagues_twitch.js` et laissez la fenêtre du terminal ouverte, pour que le bot se connecte à Twitch.
En fonction de votre version de Node.js, il sera peut-être nécessaire de taper plutôt `node --experimental-modules georgeslasaucisseblagues_twitch.js`.

# 2. Redémarrer un bot.

Par défaut, il est nécessaire de redémarrer un bot à chaque redémarrage de l'ordinateur ou du serveur. Il suffit de se rendre dans le dossier GeorgesLaSaucisseBlagues-master depuis le terminal, de lancer la commande ```node NOM_DU_FICHIER_DU_BOT.js``` (ou `node --experimental-modules NOM_DU_FICHIER_DU_BOT.js`) et de laisser la fenêtre du terminal ouverte. Pour le couper, il faut fermer la fenêtre du terminal (ou utiliser le raccourci Ctrl-C). Pour lancer plusieurs bots, il est nécessaire de laisser plusieurs fenêtres de terminal ouvertes ou d'utiliser un gestionnaire de tâches supplémentaire comme [pm2](https://pm2.keymetrics.io/).
